
[link to host website on AWS S3](http://personalyg229.s3-website-us-east-1.amazonaws.com/)
# Individual Project 1
For the individual project 1, the overgoal is to continue delivery on the personal website, deploy on GitLab and Host on AWS S3. Compared to the contents in mini-project1, all the projects' (up to week) info and related workflow, documentations are added. In addition to the name of each project, now the desciprtion are included under each tag. 

Hosting on AWS S3 - Provide robust, highly available infrastructure, potentially for a larger scale project, and require integration with various other cloud services, here are summary of hosting on AWS S3's advantages: 
- Scalability: AWS S3 can handle large amounts of traffic and data, scaling automatically to meet demand.
- Cost: Charges are based on storage, number of requests, and data transferred out of AWS. It can be cost-effective for small sites but might be expensive for high traffic sites.
- Customization: Offers more control over the hosting environment, including fine-tuned permissions, server-side encryption, etc.
- Geographic Distribution: You can use Amazon CloudFront (a content delivery network) in conjunction with S3 to serve your website with low latency worldwide.
- Integration: Easily integrates with other AWS services, which can be advantageous if you're already using AWS infrastructure.
- Reliability: AWS has a high durability and availability track record, making it reliable for mission-critical websites.

In addition to deploy on AWS S3, the project is also deployed on GitLab using CI/CD: [Link to live Gitlab static site](https://ids721-week-1-mini-project-yanboguan-a6cbd3f04e26e6e3cf2a1001f0.gitlab.io)

## Author

Yanbo Guan

## Overview of the website
Please click on the Projects button on the top-right corner of the page to navigate the project contents. Home button is located on the top left corner

### Home Page

- Introduction of the course, and brief overview of the course
![Home Page](home_page.png)

### Project Page

-  Include a list of projects, now the all the projects are updated which include a description tag for each tag, also the dark mode is available!
![Project list](project_page.png)

### Project Contents

-  Details and GitLab repo for the project
![Project Content](1.png)
![Project Content](2.png)

### Here is the example content of project
![Example Project Content](3.png)

### About

-  Introduction of myself
![About](about.png)

### Hosting on AWS S3
1. Create an AWS S3 Bucket
Log in to the AWS Management Console and navigate to the S3 service.
Click Create bucket. Give it a unique, DNS-compliant name and select a region.
Uncheck "Block all public access" settings and acknowledge that the bucket will be public.
Click Create bucket.
2. Enable Static Website Hosting
Once the bucket is created, click on its name to open it.
Navigate to the Properties tab.
Find the Static website hosting option and click Edit.
Select Enable and fill in the index document as index.html. You can also set an error document if you wish (e.g., 404.html).
Click Save changes.
3. Configure Bucket Policy for Public Access
Go to the Permissions tab of your bucket.
Find the Bucket policy section and click Edit.
Enter a policy that grants public read access to the bucket. Here’s an example policy (replace your-bucket-name with the actual name of your bucket):
```{```
  ```"Version": "2012-10-17",```
  ```"Statement": [{```
   ``` "Sid": "PublicReadGetObject",```
   ``` "Effect": "Allow",```
   ``` "Principal": "*",```
    ```"Action": "s3:GetObject",```
   ``` "Resource": "arn:aws:s3:::your-bucket-name/*"```
 ``` }]```
```}```
Click Save changes.
4. Upload Your Zola Website
Build your Zola website locally to generate the static files (if you haven't already done so).
In the AWS S3 bucket interface, click the Upload button.
You can drag and drop your website files (from the public directory created by Zola) or click Add files/Add folder to upload them.
Click Upload to start the process.
5. Access Your Website
Once the files are uploaded, go back to the Properties tab and find the Static website hosting section.
Click on the provided Endpoint URL to access your Zola website. This URL will serve your website directly from the S3 bucket.

- Here is the corresponding screenshot:
![s3](4.png)
