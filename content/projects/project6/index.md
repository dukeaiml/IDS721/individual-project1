+++
title = "mini-project6"
date = 2024-03-08
description = "Instrument a Rust Lambda Function with Logging and Tracing"
[extra]
summary = "This is a brief description of my first project."
+++
- Link to mini-project 6: [GitLab repo](https://gitlab.com/dukeaiml/IDS721/mini-project6-yg229.git )

# yg229_mini_project_6

In this project, I implemented a AWS Lambda function that designed to increment and report the number of times a website (or a specific page) has been accessed. It uses AWS DynamoDB for storage, it uses AWS Lambda with the lambda_http crate to handle web requests and MySQL for database interactions. The logging functionality within the Lambda is set up using the tracing and tracing-subscriber crates. The modification is based on the week5's mini project.

## Author

Yanbo Guan yg229

## Configuration and Initialization:
1. In cargo,toml, add the following dependecies:
```tracing = { version = "0.1", features = ["log"] }
tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }``` 
2. Go to Monitoring and Configuration Tools -> Enable: X-Ray active tracing, Enable: Lambda Insights enhanced monitoring
3. Here is the screenshot after configuration.
![](1.png)

## Lambda function summary
1. Dependencies and Setup: The code imports necessary crates for DynamoDB interaction (aws_sdk_dynamodb), AWS configuration loading (aws_config), Lambda function runtime (lambda_runtime), JSON handling (serde_json), and data serialization (serde).

2. Main Function: The main async function sets up the Lambda function using lambda_runtime::run with a handler function named lambda_handler.

3. Lambda Handler Function: lambda_handler is an asynchronous function that gets triggered upon Lambda invocations. It doesn't use the event data (LambdaEvent<Value>) directly but proceeds to increment and retrieve the count of website accesses.

4. Increment and Get Count Function: increment_and_get_count asynchronously communicates with a DynamoDB table named testdb. It updates an item identified by the key yg229 with the value site_access_count. The function increments an access_count attribute for this item, ensuring the count starts at 0 if it does not exist. It then retrieves and returns the new count as an integer.

5. DynamoDB Interaction: The DynamoDB client is configured with environment settings and is used to perform an update_item operation. This operation uses an update_expression to increment access_count or initialize it to 0 if it's not present. It specifies returning the updated values, from which the access_count attribute's new value is extracted and returned.

6. Response Construction: The Lambda function responds with a message indicating the total number of viewers, incorporating the dynamically updated count from DynamoDB.

## Deploy
1. ```cargo lambda build --release```
2. ```cargo lambda deploy```
![](2.png)
3. Add permissions for the role created (Aminastrator access if needed)
3. Add API Gateway
![](3.png)

## Results
To test the functionality of logging, go to CloudWatch.
Here are screenshots demonstrate the logging functions:
![](4.png)