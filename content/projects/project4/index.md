+++
title = "mini-project4"
date = 2024-02-23
description = "Containerize a Rust Actix Web Service"
[extra]
summary = "This is a brief description of my first project."
+++
- Link to mini-project 4: [GitLab repo](https://gitlab.com/dukeaiml/IDS721/mini-project4-yg229.git  )


# yg229_mini_project_4

In this project, I containerize a Rust Actix Web Service which take process a list and then output the sum.

## Author

Yanbo Guan

## Configuration and Initialization:
### Install Docker (I am working on mac OS) [link](https://www.docker.com/get-started/)

1. Create a new Rust Project:
```cargo new actix_web_app```
2. Navigate to the project directory:
```cd actix_web_app```
3. Add dependencies to Cargo.toml file.
4. Develop application code in the src/main.rs file
5. Build and run: ```cargo run```
6. Containerize the Rust Actix Web App: 
![](dockerfile.png)
7. Run the following command in the same directory as your Dockerfile:
```docker build -t actix_web_service .```
8. Run the Container locally:
```docker run -p 8080:8080 actix_web_service```
9. Access the webservice at http://localhost:8080
![](demo.png)